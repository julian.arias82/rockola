package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
    
    //Atributos
    String url = "jdbc:mysql://localhost:3306/rockola";
    String user = "root";
    String password = "clave";
    
    //Constructor
    public Conexion(){
         // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto

    }
    
    //Métodos
    
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection(this.url,this.user,this.password);
        System.out.println("Connection is succesful to the database"+url);
        return connection;
    }
}
