package rockolaProject;

public class Cancion {
    //Atributos
    private int id;
    private String nombre;
    private String genero;
    private int año;
    private String duracion;
    private String grupo;
    
    
    //Constructor

    public Cancion(int id, String nombre, String genero, int año, String duracion, String grupo) {
        this.id = id;
        this.nombre = nombre;
        this.genero = genero;
        this.año = año;
        this.duracion = duracion;
        this.grupo = grupo;
    }
    
    //Métodos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    @Override
    public String toString() {
        return "Cancion{" + "id=" + id + ", nombre=" + nombre + ", genero=" + genero + ", a\u00f1o=" + año + ", duracion=" + duracion + ", grupo=" + grupo + '}';
    }
    

}
