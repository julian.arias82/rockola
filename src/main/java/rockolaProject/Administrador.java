package rockolaProject;

import conexion.Conexion;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;



public class Administrador {
    //Atributos
    private int id;
    private String permisos;
    private String nombre;
   
    
    //Constructor

    public Administrador(int id, String permisos, String nombre) {
        this.id = id;
        this.permisos = permisos;
        this.nombre = nombre;
    }
    public Administrador(){
        
    }
    
    //Métodos

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermisos() {
        return permisos;
    }

    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    
    //CRUD
    //Método Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        Conexion conex = new Conexion();
        String sql = "INSERT INTO administradores(id,permisos,nombre) VALUES("+getId()+",'"+getPermisos()+"','"+getNombre()+"');";
        Connection con = conex.iniciarConexion();
        Statement statement = con.createStatement();
        statement.execute(sql);
        
    }
    
    //Método Consultar
    public void consultar() throws ClassNotFoundException, SQLException{
        Conexion conex = new Conexion();
        String sql = "SELECT id,permisos,nombre FROM administradores";
        Connection con = conex.iniciarConexion();
        Statement statement = con.createStatement();
        statement.execute(sql);
    }
    
    //Método Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException{
        Conexion conex = new Conexion();
        String sql = "UPDATE administradores SET nombre='"+getNombre()+"', permiso='"+getPermisos()+"' WHERE id="+getId()+";";
        Connection con = conex.iniciarConexion();
        Statement statement = con.createStatement();
        statement.execute(sql);
    }    

    //Método Borrar
    public void borrar() throws ClassNotFoundException, SQLException{
        Conexion conex = new Conexion();
        String sql = "DELETE FROM administradores WHERE id="+getId()+";";
        Connection con = conex.iniciarConexion();
        Statement statement = con.createStatement();
        statement.execute(sql);
        }
    
    @Override
    public String toString() {
        return "Administrador{" + "id=" + id + ", permisos=" + permisos + ", nombre=" + nombre + '}';
    }
    
    

    
    

}
